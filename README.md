# README

This repository is for the learning of a programming language " Python " .

# Contents

- helloPython
  「こんにちは」する
- flaskLesson
  「Flask」軽量Webフレームワークらしい
- electronLesson
  「Electron」Web技術でデスクトップアプリを作るやーつ
- addressSearch
  WebAPIをつかって、外部ファイルを絡めたプログラム

# pipのインストール

## get-pip.pyをDL

`https://bootstrap.pypa.io/get-pip.py`

## インストール

`sudo python get-pip.py`

# Flaskのインストール

## pipでインストール

`sudo pip install --upgrade Flask`

# electron-prebuiltのインストール

`sudo npm install -g electron-prebuilt`

## electron-packagerパッケージのインストール

`sudo npm i electron-packager -g`

