#coding: UTF-8

from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/")
@app.route("/<message>")
def index(message=None):
    # 「templates/index.html」のテンプレートを使う
    # 「message」という変数に"Hello"と代入した状態で、テンプレート内で使う
    return render_template('index.html', message=message)

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5000)