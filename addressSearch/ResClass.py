#coding: UTF-8

class ResClass:
  """API response result info class"""

  def __init__(self):
    self.code = '0000'
    self.message = ''

  def __init__(self, code, message=''):
    self.code = code
    self.message = message

  def getCode(self):
    return self.code

  def setCode(self, code):
    self.code = code

  def getMessage(self):
    if self.message != '':
      return self.message

    if self.code == '0001':
      return 'success'
    elif self.code == '9001':
      return 'no return json'
    elif self.code == '9002':
      return 'error response'

    return 'success'