#coding: UTF-8

import sys
import urllib2, json
from func import *
from ResClass import *

param = sys.argv

# 引数から郵便番号を取得
postalCode = receiveArg(param)

if postalCode:
  # APIを呼び出し
  resClass = callApi(postalCode)

  # APIの結果を判別して出力
  import re
  if re.match('9' , resClass.getCode()):
    print resClass.getMessage()
  else:
    location = resClass.getMessage();

    print location['prefecture']
    print location['city']
    print location['town']
else:
  print "Please input postalCode."

