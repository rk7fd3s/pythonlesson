#coding: UTF-8

import re
import urllib2, json
from ResClass import *
from werkzeug._compat import to_bytes

apiBaseUri = "http://geoapi.heartrails.com/api/json?method=searchByPostal"

def receiveArg(params):
  if len(params) > 1:
    postalCode = params[1]

    if re.match('\d{7}' , postalCode):
      return postalCode

  return False

def callApi(postal):
  apiUri = apiBaseUri + '&postal=' + postal

  res = urllib2.urlopen(apiUri)
  jsonRoot = json.loads(res.read())

  # jsonが空であれば、エラーとして返却
  if jsonRoot == '':
    return ResClass('9001')

  # response項目を取得
  response = jsonRoot['response'];

  # error項目があれば、エラーとして返却
  if response.has_key('error'):
    return ResClass('9002', response['error'])

  # location項目を取得
  location = response['location'];
  # リストが格納されているので、最初の要素だけ取得
  loco = location[0]

  # 正常処理として、取得したデータを返却
  return ResClass('0000', loco)


